| branch | build status |
| ---    | ---          |
| master | [![master status](https://circleci.com/bb/octamis/ta-metricator-for-nmon/tree/master.svg?style=svg)](https://circleci.com/bb/octamis/ta-metricator-for-nmon/tree/master)

# TA-metricator-for-nmon

Copyright 2017-2019 Octamis - Copyright 2017-2019 Guilhem Marchand

All rights reserved.
